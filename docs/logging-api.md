---
id: logging-api
title: Logging API
sidebar_label: Logging API
---

DV offers a logging facility. Log messages get written to the DV log file (typically in `~/.dv-logger.txt`), as well as published to any attached clients, such as the GUI. Logs get automatically tagged with the module name as well as the timestamp.


## Accessing the log object

Every DV module class extends from `dv::ModuleBase`. This provides the module with an object called `log`, that can be accessed from everywhere in the class, including constructor and destructor. 


## Log levels

Every log level has a member in the log object. To log to different levels, use

```C+++
log.debug << "This is a debug message" << dv::logEnd;
log.info << "This is an info message" << dv::logEnd;
log.warning << "This is a warning message" << dv::logEnd;
log.error << "This is an error message" << dv::logEnd;
log.critical << "This is a critical message" << dv::logEnd;
```

All logging methods described below, can be applied to all log levels.

## Logging syntax

### Simple logging

The simplest logging syntax can be achieved with

```C++
log.info("This is an info string");
```

DV logging accepts any type that `std::cout` accepts. 

```C++
log.info(12);
```

### Stream logging

Log messages can be streamed into the logging object. Every object that is streamed in appends to the current log message. To send the message out and start a new message, stream in `dv::logEnd`.

```C++
log.info << "The answer to answer is " << 42 << dv::logEnd;
```

Messages are not complete until `dv::logEnd` is streamed in

```C++
log.info << 1 << " + " << 1 << " = ";
log.info <<  (1 + 1) << dv::logEnd;
```


### Formatted logging

DV supports formatting log messages with *printf* syntax. Use the function `format` for this purpose

```C++
double mean;
double stddev;
int nSamples;

log.info.format("mean: %.2f stddev: %.2f samples: %d", mean, stddev, nSamples);
```

Consult printf documentation for the format syntax.
