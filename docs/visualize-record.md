---
id: visualize-record
title: Visualize and Record
sidebar_label: Visualize and Record
---

## Starting DV

If you own a DVS camera, plug it in, screw in the lens and do not forget to remove the lens cap.

Start DV with
* **Windows** Double click the DV icon on the Desktop
* **macOS** Search DV in spotlight
* **Linux** Execute `dv-gui` in the command line or select `Development -> DV` in the system menu

## Select input device
DV should automatically open and display the output of your connected camera. Should DV fail to find the correct camera, you can
selct the input device in the *Capture* Section of the right bar

![Input device selection](assets/select-input.png)

## Adjusting camera settings

#### Priority options
Your iniVation camera has many settings and biases that can be adjusted. The most important settings can be found in the right bar.

![Main camera settings](assets/camera-settings-1.png)

#### Non-priority options
All camera settings can be accessed by clicking on the **+** next to the camera name (shown with the purple annotation in the screenshot above).

A pop-up with all available settings appears. Any setting can be changed right from within this popup.
Specific settings can be searched for by typing in the name of the setting in the search bar.

![All camera settings](assets/camera-settings-modal.png)

#### Add / remove an option to priority options
Any option that can be added to priority options has a **+** button to the right. Clicking this button adds the setting to the list of settings accessible from the right bar in the main window.

To remove an option from priority options, either click the **-** button to the right of the option, or right click on the option and select *Remove from priority option*.

The order of priority options can be changed by right-clicking on the option.

## Adjusting visualization options
The default *Visualize* configuration comes with two software modules enabled by default:

* A **Noise Filter** for the events output
* An **Accumulator** for basic frame reconstruction from events

The settings for both of these modules can be adjusted the same way as the camera module.

#### Example: Disable accumulation decay
By default, the accumulator module decays the contribution of old events over time as soon as new events for a pixel arrive.
This is, by default, done in an exponential manner.

To switch decay off, click the **+** button next to *Accumulator*.

![Accumulator settings](assets/accumulator-settings.png)

Switch the setting for *Decay function* from *Exponential* to *None*. One can see that the accumulated image is a better representation of reality, but also accumulates more noise over time.

## Record data

Select the *Record* configuration in the left sidebar of DV. The current configuration gets replaced with a standard recording configuration. The default record configuration records all events, frames, imu as well as trigger data from a connected DVS / Davis camera. 

![UI element positions for recording](assets/record-ui.png)

Recording can be started by clicking on the button with the red circle. Every recording creates a new file, by default, in your home directory. All data is recorded in the `aedat 4.0` format. Files in that format can be read in again by DV, as well as with our [dv-python](https://gitlab.com/inivation/dv-python) scripts.

## Play back data
 
 To play back a file, choose the *Playback* configuration in the left bar. After the configuration is loaded, one can choose the file from the *File* option in the right hand bar. 
