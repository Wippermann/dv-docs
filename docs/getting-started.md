---
id: getting-started
title: Get Started
sidebar_label: Get started
---

![DV software screenshot](assets/screen.png)

**DV** is the software for the [iniVation **Dynamic Vision Sensors (DVS/DAVIS)**](https://inivation.com/buy). DV connects to your camera and shows its output. It is also the official Software Development Kit to write application software for event-based cameras.

## Download

### Windows / Mac
<!-- **You do not need to own a DVS camera** in order to play around with event-based vision. DV comes with some built-in recordings.
DV runs on Windows, macOS and Linux. For development of custom code, macOS or Linux are currently required. -->

#### Stable
<a class="button" href="http://release.inivation.com/gui/latest-win-stable">
    ![Windows](assets/icons/windows.svg) Download for Windows (64-bit)
</a>
<a class="button" href="http://release.inivation.com/gui/latest-mac-stable">
    ![macOS](assets/icons/apple.svg) Download for macOS (64-bit)
</a>

#### Bleeding edge releases

**Note** Bleeding edge releases are untested and are potentially broken. Use at your own risk or when directed to do so by our support department.

* [Latest Git master Windows build (64-bit)](http://release.inivation.com/gui/latest-win-master)
* [Latest Git master macOS build (64-bit)](http://release.inivation.com/gui/latest-mac-master)

#### Development

If you plan to develop your own modules on a mac, the development runtime is required. The easiest way to install it is via [Homebrew](https://brew.sh/).
We provide a [Homebrew tap](https://gitlab.com/inivation/homebrew-inivation/) for macOS. Install the development runtime with:

```bash
brew tap inivation/inivation
brew install libcaer --with-libserialport --with-opencv
brew install dv-runtime
```

You can also pass `--HEAD` to build from the latest Git master automatically.

We do not currently support development of new modules on Windows.


### Arch Linux

You can find DV in the AUR repository, install the package 'dv-gui', which depends on all the components of the DV software platform.

The standard packages in the AUR repository already include all development files.
We also offer a dv-runtime-git package that builds the latest Git master automatically, in case you need the latest fixes and functionality.


### Fedora Linux

We provide a [COPR repository](https://copr.fedorainfracloud.org/coprs/inivation/inivation/)
for Fedora 29 and 30 on the x86_64, x86, arm64 and ppc64 architectures.
Please note that dv-gui is only available on x86_64, the dv-runtime on all supported architectures.

Please execute the following commands to use it:

```bash
sudo dnf copr enable inivation/inivation
sudo dnf install dv-gui
```

If you plan to develop your own modules, please install the following additional package:

```bash
sudo dnf install dv-runtime-devel
```


### Gentoo Linux

A valid Gentoo ebuild repository is available [here](https://gitlab.com/inivation/gentoo-inivation/)
over Git. The package to install is 'dev-util/dv-gui'.

The standard packages in the Gentoo ebuild repository already include all development files.
We also offer a =dev-util/dv-runtime-9999 package that builds the latest Git master automatically, in case you need the latest fixes and functionality.

### Ubuntu Linux

We provide a [PPA repository](https://launchpad.net/~inivation-ppa/+archive/ubuntu/inivation)
for Ubuntu Xenial (16.04 LTS), Bionic (18.04 LTS) and Disco (19.04)
on the x86_64, x86, arm64 and armhf architectures (arm64/armhf only supported on Disco).
Please note that dv-gui is only available on x86_64, the dv-runtime on all supported architectures.

On Ubuntu Xenial (16.04 LTS), please note that you will have to add a couple of additional PPA repositories to get the versions of the dependencies we require:

```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo add-apt-repository ppa:lkoppel/opencv
sudo add-apt-repository ppa:janisozaur/cmake-update
```

Please execute the following commands to use it:

```bash
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt-get update
sudo apt-get install dv-gui
```

If you plan to develop your own modules, please install the following additional package:

```bash
sudo apt-get install dv-runtime-dev
```


## Requirements

#### Visualization / Recording
* **A computer** running either Windows, macOS or Linux
<!-- * **A [DVS camera](https://inivation.com/buy)** for real data. A camera is not strictly required, DV contains some prerecorded scenes and is capable of reading any `.aedat` file. -->

#### Development
* **A computer** running macOS or Linux
* Knowledge about C++ programming
