---
id: time-surface
title: Time Surface
sidebar_label: Time Surface
---

Header `<dv-sdk/processing/frame.hpp>`

Time surface functions. A time surface is a 2D array that holds a timestamp in every pixel. Timestamps in DV are 64bit signed integers. Since *OpenCV* does not provide a 64bit integer type, DV provides a Matrix type to store timestamp information

## Time Mat

A `TimeMat` is very similar to the OpenCV `Mat` type, with the difference that every pixel is a 64bit signed integer, a data type OpenCV does not offer. Just like OpenCV `Mat`, copies of TimeMat are shallow.

### Creating a new Time Mat

To create a new `TimeMat`

```C++
dv::TimeMat TimeMat(rows, cols);

// alternatively
dv::TimeMat TimeMat(cv::Size(width, height));
```

### Adding, subtracting constants to Time Mat

To add a constant to a Time Mat in place

```C++
// add constant 1 to TimeMat `timeMat`
timeMat += 1;

// subtract constant 1 from TimeMat `timeMat`
timeMat -= 1;
```

To add / subtract without modifying the original TimeMat

```C++
// Create a new timeMat with the same content as the old map but +1
dv::TimeMat subtracted = timeMat + 1;


// Create a new timeMat with the same content as the old map but -1
dv::TimeMat subtracted = timeMat - 1;
```
Note that this operation creates a copy.


### Getting OpenCV Mat from TimeMat

A `TimeMat` can be converted into an OpenCV `Mat` with a requested datatype.
Note that this is in general unsafe, since the data range of a 64bit integer
is much higher than for the OpenCV provided types.

To convert to an OpenCV Matrix

```C++
// convert to a 32bit integer `Mat`
cv::Mat intMat = timeMat.getOCVMat<int32_t>();
// convert to a double integer `Mat`
cv::Mat doubleMat = timeMat.getOCVMat<double>();
// convert to a double integer `Mat`
cv::Mat floatMat = timeMat.getOCVMat<float>();
```

## Time Surface

A Time Surface is a 2D frame where every pixel holds the timestamp of the latest event that happened at that pixel. 

### Setup a Time Surface

To create a new Time Surface

```C++
dv::TimeSurface TimeSurface(rows, cols);

// alternatively
dv::TimeSurface TimeSurface(cv::Size(width, height));
```

### Updating Time Surface

To update the time surface with a store of events

```C++
timeSurface.accept(eventStore);
```

### Getting the Time Mat

```C++
dv::TimeMat surface = timeSurface.getTimeSurface();
```