---
id: getting-ready-for-development
title: Get ready for development
sidebar_label: Get ready for development
---

## Make sure everything is installed

In order to develop modules for DV, you need a working C++ 17 toolchain installed on your system.
Recent version of GCC and Clang, version 7.0 or newer, are fine and found on most current systems.

### macOS

The recommended toolchain is the standard toolchain provided by Apple with XCode. To install this, run:

```bash
xcode-select --install
```

We also require *cmake* to build applications. The easiest way to install *cmake* is via [Homebrew](https://brew.sh). Run:

```bash
brew install cmake
```

The DV.app bundle that you downloaded, does contain a built-in runtime (the runtime that gets started automatically when you open DV). However, this bundled runtime does not contain the development headers, and is not linked against your local libraries. Therefore, we install a dv-runtime into your path in `/usr/local`. The easiest way to do so is with brew

```bash
brew tap inivation/inivation
brew install libcaer --with-libserialport --with-opencv
brew install dv-runtime
```

### Linux

Make sure you have a current build pipeline, including *cmake* installed on your system.

## The purpose of modules

A DV module is a piece of code that applies some sort of operation to data, usually having multiple data inputs and outputs. Some modules only have outputs, such as the camera or file playback modules, as they only produce new data for the framework to consume. Some modules only have inputs, such as the network or file recorder modules, as they send data outside of the framework.
When you want to build an application for DV, you most likely want to build a module that takes on some camera data, and outputs some measurements or visualization outputs.

## The anatomy of a module

Modules are shared libraries. They are files with the extension *.so* (Linux), *.dylib* (macOS), or *.dll* (Windows). Modules library files are in:

* `/usr/share/dv/modules` (Linux)
* `/usr/local/share/dv/modules` (macOS)
* `C:\Program Files\DV\runtime\dv_modules` (Windows)

For a library to be discoverable by DV, it has to be in that directory.

## Compiling a demo module

A demo module that counts events can be found on Gitlab. *(https://gitlab.com/inivation/dv-example-cpp)*.
Every module is essentially a CMake project, that most modern IDEs understand.

To test your toolchain, you can try to compile the demo module with the following commands. If everything goes well, you should end up with a file `dv-example-cpp.(so|dylib)` in the modules directory.

```bash
git clone https://gitlab.com/inivation/dv-example-cpp.git
cd dv-example-cpp
cmake .
make -j2 -s
sudo make install
```

