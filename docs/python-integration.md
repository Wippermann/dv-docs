---
id: python-integration
title: Python Integration
sidebar_label: Python Integration
---


DV has an integration with python with allows to

* Stream data from DV to a Python program
* Open files recorded with DV (`*.aedat4`)
* Open certain files recorded with third party software

Full documentation available in the gitlab repository

<a class="button" href="https://gitlab.com/inivation/dv-python">
    DV-PYTHON documentation
</a>
