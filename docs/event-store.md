---
id: event-store
title: Event Store
sidebar_label: Event Store
---

Header `<dv-sdk/processing/core.hpp>`

The `EventStore` class is the base container class for the event processing API. It behaves a lot like the `cv::Mat` class of *OpenCV*.
An `EventStore` contains a collection of `dv::Event`, monotonically increasing. 

Event Store copies are implemented as shallow copies.

## Creating an EventStore

An `EventStore` can be created the normal C++ way
```C++
// Create new EventStore
dv::EventStore store;
// Add an event to the event store
store.add(dv::Event(x, y, polarity, timestamp));
```


## Converting input data to EventStore

The event data from a module input can automatically be converted to an `EventStore`

```C++
dv::EventStore store = inputs.getEventInput("events").events();
```

Input data can also be appended to an event store

```C++
// first option
store.add(inputs.getEventInput("events").events());
// second option
store += inputs.getEventInput("events").events();
```

**Note:** Event store is implemented as shallow copy objects. Converting input data to an event store or adding it to an existing store does only reference, but not copy any data. DV makes sure that memory is managed correctly.

## Slicing Event Stores

An `EventStore` can be sliced by time or by the number of events. Slicing is a shallow operation, it does not copy any data. Slicing returns a new `EventStore` that only references the data requested. The original `EventStore` is unaffected.

### Slicing by time

To get all events that happened in a time interval
```C++
// startTime and endTime in microsecond timestamps
EventStore intervalStore = store.sliceTime(startTime, endTime);
// No data was copied. original `store` is unffected.
```

To get all data from a specific timestamp until the end of the store

```C++
dv::EventStore eventsAfterTimestamp = store.sliceTime(timestamp);
```

To get all data that happened in the last x microseconds, use a negative number
for sliceTime

```C++
dv::EventStore eventsLast10000microseconds = store.sliceTime(-10000);
```

**Note** Since no data is copied, these functions are fairly performant. A slice in time has computational complexity `O(log(n))` with `n` the number of elements in the store.



### Slicing by number of events

To get all events in a number-of-events interval 

```C++
dv::EventStore intervalStore = store.slice(startIndex, endIndex);
```
This is a shallow operation. No data is copied. The original store is unaffected.
Note that the indices are including the first index, excluding the last index.

To get all events from a specific index to the end

```C++
dv::EventStore eventsAfterIndex = store.slice(startIndex);
```

To get the last n events, use a negative index

```C++
dv::EventStore last100Events = store.slice(-100);
```

## Iterating over Event Stores

`EventStore` exposes a bidirectional iterator

```C++
for (const dv::Event &event : store) {
    // process event
}
```


## Combining Event Stores

Event stores can be added together

```C++
dv::EventStore combinedStore = store1 + store2;
```
No data is copied in this operation. `combinedStore` references both the data from `store1` and `store2`. Both original stores are unaffected. The first timestamp of `store2` must be higher or equal than the last timestamp of `store1` to preserve monotonicity.


To add the contents of one event store to another event store do
```C++
store1 += store2;
```
`store1` gets modified and references the data from both stores afterwards. No data is copied.

## Adding events to Event Stores

To add an event to an event store 

```C++
store += dv::Event(x, y, polarity, timestamp);
```

## Sending Event Stores to an output

To append the contents of an Event Store to an output in your module, just pipe it to the output

```C++
outputs.getEventOutput("events") << store;
```
Make sure that in order to send the output to the next module, a commit is required. Details in are in the Module I/O documentation


## Getting bounds of Event Stores

To get the higher and lower time bounds of the data in an event store

```C++
int64_t highestTime = store.getHighestTime();
int64_t lowestTime = store.getLowestTime();
```
Note that highest / lowest times correspond to the timestamps of the first / last events in the store. 

To check if a store is empty and to get the number of events in the store

```C++
size_t numberOfElements = store.getSize();
bool isEmpty = store.isEmpty();
```

## Removing Events from Event Stores

`EventStore` does not provide any functionality for removing events. Instead, use the slicing functionality and let the original store go out of scope. This way, the memory gets released

To remove the first n microseconds from an Event Store

```C++
// remove first 1000 microseconds
store = store.sliceTime(1000);
// this re-assigns store. The old store gets out of scope. This will release the data that
// is no longer referenced by the new store.
```

To remove the first n events from an Event Store

```C++
// remove first 1000 events
store = store.slice(1000);
```

To remove interval between two timestamps from a store, slice the part from the front and the parts from the back, and add them together again. As always, no data is copied in this operation.

```C++
store = store.sliceTime(0, timestamp1) + store.sliceTime(timestamp2);
```
