---
id: update-firmware
title: Update camera firmware
sidebar_label: Update camera firmware
---

## Update procedure

The software running on the camera can be updated as follows:

1. Open **DV**
2. Select *Maintenance -> Update camera firmware*
3. Wait for the firmware update window to appear

![Flashy update window](assets/flashy-update.png)

4. If there is an update available, the tool will display the update. If the newest firmware version is not listed, you may want to update DV
5. If available, click *Update firmware* and *Update logic*
6. **Unplug and re-plug the camera**

