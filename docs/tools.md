---
id: tools
title: Tools
sidebar_label: Tools
---

Header `<dv-sdk/processing/event.hpp>`

A number of processing tools are available for common Event processing tasks

## Filter ROI

To filter out a region of interest

```C++
cv::Rect roi(x, y, width, height);
dv::EventStore outStore;
dv::roiFilter(inStore, outStore, roi);
```

Extracts only the events that are within the defined region of interest.
This function copies the events from the in EventStore into the given out EventStore, if they intersect with the given region of interest rectangle.


## Scale

Subsample a set of events from a x / y range to another x / y range

```C++
dv::EventStore outStore;
dv::scale(inStore, outStore, xFactor, yFactor);
```
Projects the event coordinates onto a smaller range. The x- and y-coordinates the divided
by xFactor and yFactor respectively and floored to the next integer.


## Filter polarity

To filter out all events with a specific polarity

```C++
dv::EventStore outStore;

// to only keep the on events
dv::polarityFilter(inStore, outStore, true);

// to only keep the off events
dv::polarityFilter(inStore, outStore, false);
```

## Getting a Bounding Rect for a set of events

To compute a bounding rect that includes all events in a set of events

```C++
cv::Rect rect = dv::boundingRect(inStore);
```

