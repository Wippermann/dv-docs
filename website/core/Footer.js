/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  docUrl(doc) {
    const baseUrl = this.props.config.baseUrl;
    const docsUrl = this.props.config.docsUrl;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    return `${baseUrl}${docsPart}${doc}`;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? `${language}/` : '') + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <img
                src={this.props.config.baseUrl + this.props.config.footerIcon}
                alt={this.props.config.title}
                width="66"
                height="58"
              />
            )}
          </a>
          <div>
            <h5>Docs</h5>
            <a href={this.docUrl('getting-started', this.props.language)}>
              Getting Started
            </a>
            <a href={this.docUrl('getting-started', this.props.language)}>
              Download
            </a>
            <a href={this.docUrl('module-api', this.props.language)}>
              API Reference
            </a>
          </div>
          <div>
            <h5>Resources</h5>
            <a href="https://gitlab.com/inivation"
            target="_blank"
            rel="noreferrer noopener">
              Gitlab
            </a>
            <a
              href="http://inivation.com/buy"
              target="_blank"
              rel="noreferrer noopener">
              Buy a DVS
            </a>
          </div>
          <div>
            <h5>More</h5>
            <a href="https://discordapp.com/">Careers</a>
          </div>
        </section>

      
        <section className="copyright">{this.props.config.copyright}</section>
      </footer>
    );
  }
}

module.exports = Footer;
